using TMPro;

namespace halfmod
{
    public static class consts
    {
        public static readonly TMP_Glyph[] glyphData = new[]
        {
            new TMP_Glyph()
            {
                id = 32,
                x = 6,
                y = 2053,
                width = 0,
                height = 0,
                xOffset = 0,
                yOffset = 0,
                xAdvance = 131,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 33,
                x = 441,
                y = 436,
                width = 137.1875f,
                height = 256.375f,
                xOffset = 23f,
                yOffset = 206.25f,
                xAdvance = 211.3125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 34,
                x = 1968,
                y = 888,
                width = 67.75f,
                height = 73.125f,
                xOffset = 15.3125f,
                yOffset = 191.9375f,
                xAdvance = 98.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 37,
                x = 1116,
                y = 1374,
                width = 218,
                height = 205.4375f,
                xOffset = -25.625f,
                yOffset = 180.4375f,
                xAdvance = 243.5625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 38,
                x = 506,
                y = 1514,
                width = 236.9375f,
                height = 257.125f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 262.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 39,
                x = 1563,
                y = 1378,
                width = 39.875f,
                height = 63.4375f,
                xOffset = -25.625f,
                yOffset = 201.375f,
                xAdvance = 39.9375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 40,
                x = 1892,
                y = 1782,
                width = 145.8125f,
                height = 259.1875f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 197,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 41,
                x = 574,
                y = 974,
                width = 150.6875f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 201.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 43,
                x = 1551,
                y = 719,
                width = 199.5625f,
                height = 246.625f,
                xOffset = 4.0625f,
                yOffset = 201.625f,
                xAdvance = 240,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 44,
                x = 1968,
                y = 814,
                width = 50.625f,
                height = 62.6875f,
                xOffset = -25.625f,
                yOffset = 49.125f,
                xAdvance = 76.25f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 45,
                x = 738,
                y = 1458,
                width = 211.625f,
                height = 43.25f,
                xOffset = 0,
                yOffset = 103.375f,
                xAdvance = 237.1875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 46,
                x = 1563,
                y = 1453,
                width = 44.5f,
                height = 48.0625f,
                xOffset = -25.625f,
                yOffset = 39.1875f,
                xAdvance = 70.125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 48,
                x = 782,
                y = 434,
                width = 136.375f,
                height = 259.1875f,
                xOffset = 10.1875f,
                yOffset = 208.5625f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 49,
                x = 1563,
                y = 1107,
                width = 28.6875f,
                height = 259.9375f,
                xOffset = 73.6875f,
                yOffset = 208.3125f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 50,
                x = 1069,
                y = 401,
                width = 121.8125f,
                height = 259.9375f,
                xOffset = 25.5625f,
                yOffset = 208.3125f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 51,
                x = 6,
                y = 431,
                width = 109.25f,
                height = 260.9375f,
                xOffset = 25.5625f,
                yOffset = 208.5625f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 52,
                x = 6,
                y = 159,
                width = 174.5f,
                height = 260.1875f,
                xOffset = 0,
                yOffset = 208.5625f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 53,
                x = 1385,
                y = 432,
                width = 131.75f,
                height = 259.6875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 54,
                x = 930,
                y = 435,
                width = 127.9375f,
                height = 258.875f,
                xOffset = 0,
                yOffset = 208.5625f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 55,
                x = 1872,
                y = 1243,
                width = 158.125f,
                height = 259.1875f,
                xOffset = 0,
                yOffset = 207.5625f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 56,
                x = 986,
                y = 1514,
                width = 118.6875f,
                height = 257.625f,
                xOffset = 34.75f,
                yOffset = 207.25f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 57,
                x = 590,
                y = 378,
                width = 136.125f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 207.5625f,
                xAdvance = 204.6875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 59,
                x = 1116,
                y = 1861,
                width = 253.5625f,
                height = 180.125f,
                xOffset = -25.625f,
                yOffset = 168.625f,
                xAdvance = 279.125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 61,
                x = 1112,
                y = 940,
                width = 207,
                height = 153.25f,
                xOffset = 0,
                yOffset = 154.8125f,
                xAdvance = 232.5625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 63,
                x = 930,
                y = 167,
                width = 120.5625f,
                height = 256.625f,
                xOffset = 1,
                yOffset = 207,
                xAdvance = 262,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 65,
                x = 736,
                y = 918,
                width = 156.0625f,
                height = 258.6875f,
                xOffset = 0,
                yOffset = 207.5625f,
                xAdvance = 207.25f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 66,
                x = 6,
                y = 704,
                width = 151.75f,
                height = 259.9375f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 202.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 67,
                x = 290,
                y = 432,
                width = 139.6875f,
                height = 259.4375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 190.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 68,
                x = 441,
                y = 166,
                width = 136.375f,
                height = 258.625f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 187.5625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 69,
                x = 1892,
                y = 1514,
                width = 144.3125f,
                height = 256.125f,
                xOffset = 0,
                yOffset = 206.5f,
                xAdvance = 195.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 70,
                x = 589,
                y = 107,
                width = 133.5625f,
                height = 259.4375f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 184.75f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 71,
                x = 1536,
                y = 448,
                width = 192.125f,
                height = 259.4375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 243.3125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 72,
                x = 169,
                y = 704,
                width = 140.4375f,
                height = 259.1875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 191.625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 73,
                x = 1062,
                y = 131,
                width = 117.6875f,
                height = 258.6875f,
                xOffset = 0,
                yOffset = 207.5625f,
                xAdvance = 168.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 74,
                x = 1331,
                y = 164,
                width = 116.6875f,
                height = 256.875f,
                xOffset = 0,
                yOffset = 207,
                xAdvance = 167.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 75,
                x = 986,
                y = 1243,
                width = 117.1875f,
                height = 259.6875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 168.375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 76,
                x = 472,
                y = 704,
                width = 143.8125f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207,
                xAdvance = 194.9375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 77,
                x = 1381,
                y = 1783,
                width = 244.0625f,
                height = 258.125f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 295.25f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 78,
                x = 6,
                y = 1783,
                width = 277.125f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 328.25f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 79,
                x = 1115,
                y = 1105,
                width = 207,
                height = 257.875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 258.1875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 80,
                x = 1372,
                y = 1513,
                width = 238.1875f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 289.375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 81,
                x = 738,
                y = 1188,
                width = 206.25f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 257.375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 82,
                x = 409,
                y = 976,
                width = 154,
                height = 256.125f,
                xOffset = 0,
                yOffset = 206.75f,
                xAdvance = 205.1875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 83,
                x = 1637,
                y = 1785,
                width = 243.8125f,
                height = 256.9375f,
                xOffset = 0,
                yOffset = 206.25f,
                xAdvance = 295,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 84,
                x = 1330,
                y = 703,
                width = 194.9375f,
                height = 259.6875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 246.125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 85,
                x = 1603,
                y = 977,
                width = 200.3125f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207,
                xAdvance = 251.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 86,
                x = 584,
                y = 1783,
                width = 254.3125f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 305.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 87,
                x = 1622,
                y = 1247,
                width = 238.4375f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 289.625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 88,
                x = 209,
                y = 976,
                width = 188.3125f,
                height = 256.125f,
                xOffset = 0,
                yOffset = 206.75f,
                xAdvance = 239.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 89,
                x = 6,
                y = 975,
                width = 191.625f,
                height = 257.4375f,
                xOffset = 0,
                yOffset = 206.5f,
                xAdvance = 242.8125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 90,
                x = 754,
                y = 1513,
                width = 220.3125f,
                height = 258.6875f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 271.4375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 97,
                x = 904,
                y = 705,
                width = 156.0625f,
                height = 258.6875f,
                xOffset = 0,
                yOffset = 207.5625f,
                xAdvance = 207.25f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 98,
                x = 127,
                y = 432,
                width = 151.75f,
                height = 259.9375f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 202.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 99,
                x = 321,
                y = 704,
                width = 139.6875f,
                height = 259.4375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 190.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 100,
                x = 1237,
                y = 433,
                width = 136.375f,
                height = 258.625f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 187.5625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 101,
                x = 956,
                y = 975,
                width = 144.3125f,
                height = 256.125f,
                xOffset = 0,
                yOffset = 206.5f,
                xAdvance = 195.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 102,
                x = 734,
                y = 107,
                width = 133.5625f,
                height = 259.4375f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 184.75f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 103,
                x = 1740,
                y = 431,
                width = 192.125f,
                height = 259.4375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 243.3125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 104,
                x = 192,
                y = 160,
                width = 140.4375f,
                height = 259.1875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 191.625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 105,
                x = 1528,
                y = 178,
                width = 117.6875f,
                height = 258.6875f,
                xOffset = 0,
                yOffset = 207.5625f,
                xAdvance = 168.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 106,
                x = 1657,
                y = 163,
                width = 116.6875f,
                height = 256.875f,
                xOffset = 0,
                yOffset = 207,
                xAdvance = 167.875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 107,
                x = 1202,
                y = 162,
                width = 117.1875f,
                height = 259.6875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 168.375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 108,
                x = 627,
                y = 648,
                width = 143.8125f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207,
                xAdvance = 194.9375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 109,
                x = 1116,
                y = 1591,
                width = 244.0625f,
                height = 258.125f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 295.25f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 110,
                x = 295,
                y = 1783,
                width = 277.125f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 328.25f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 111,
                x = 1333,
                y = 974,
                width = 207,
                height = 257.875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 258.1875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 112,
                x = 6,
                y = 1513,
                width = 238.1875f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 289.375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 113,
                x = 1345,
                y = 1243,
                width = 206.25f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 257.375f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 114,
                x = 1072,
                y = 672,
                width = 154,
                height = 256.125f,
                xOffset = 0,
                yOffset = 206.75f,
                xAdvance = 205.1875f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 115,
                x = 1637,
                y = 1517,
                width = 243.8125f,
                height = 256.9375f,
                xOffset = 0,
                yOffset = 206.25f,
                xAdvance = 295,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 116,
                x = 1762,
                y = 702,
                width = 194.9375f,
                height = 259.6875f,
                xOffset = 0,
                yOffset = 208.3125f,
                xAdvance = 246.125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 117,
                x = 1815,
                y = 973,
                width = 200.3125f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207,
                xAdvance = 251.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 118,
                x = 850,
                y = 1783,
                width = 254.3125f,
                height = 258.9375f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 305.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 119,
                x = 256,
                y = 1513,
                width = 238.4375f,
                height = 258.4375f,
                xOffset = 0,
                yOffset = 207.8125f,
                xAdvance = 289.625f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 120,
                x = 209,
                y = 1245,
                width = 188.3125f,
                height = 256.125f,
                xOffset = 0,
                yOffset = 206.75f,
                xAdvance = 239.5f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 121,
                x = 6,
                y = 1244,
                width = 191.625f,
                height = 257.4375f,
                xOffset = 0,
                yOffset = 206.5f,
                xAdvance = 242.8125f,
                scale = 1
            },
            new TMP_Glyph()
            {
                id = 122,
                x = 506,
                y = 1244,
                width = 220.3125f,
                height = 258.6875f,
                xOffset = 0,
                yOffset = 208.0625f,
                xAdvance = 271.4375f,
                scale = 1
            }
        };


        public static void fixGlyphData()
        {
            
            foreach (TMP_Glyph g in glyphData)
            {
                //g.xOffset = 0;
                //g.yOffset = 0;

                //g.scale = 0.02f;
                //g.xAdvance = g.xAdvance / ;
            }
        }
    }
}