﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ModCommon;
using Modding;
using TMPro;
using UnityEngine;
using Object = UnityEngine.Object;
using TextMeshPro = On.TMPro.TextMeshPro;
using ModCommon.Util;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace halfmod
{
    public class halfmod : Mod, ITogglableMod
    {
        private AssetBundle ab;
        private TMP_FontAsset wingshadeTMP;
        public static Font wingshadeTTF;
        private Material wingshadeMat;
        private Texture2D wingshadeTex;

        private Material oldMat;
        private Texture2D oldTex;
        
        private System.Random rng = new System.Random();

        private static readonly string[] MOD_NAMES = new[]
        {
            "Lightbreaker",
            "Half-mod",
            "Soulless Light",
            "Gods and Glory", // the worst chineese ripoff of them all.
            "Not Lightbringer",
            "Radiance AI",
            "Shitmothst 2"
        };

        private static readonly string[] VERSION_NAMES = new[]
        {
            "Or Why I was Banned from Art-Discussion",
            "328-6837", // Dau-nter
            "Half-Complete",
            "Do Vessels Dream of Infected Stags?",
            "Undaunted",
            "Nuke Meme: New Vegas",
            "You made this? I made this",
            "Made in China" // The only accurate version name
        };

        public halfmod()
        {
            FieldInfo field = typeof(Mod).GetField
                ("Name", BindingFlags.Instance | BindingFlags.Public);
            field?.SetValue(this, MOD_NAMES[rng.Next(0, MOD_NAMES.Length)]);
        }

        public override void Initialize()
        {
            Log("WHAT THE FUCK WHAT THE FUCK WHAT thE FUCK SEND HELP!!OIJ@IO!@J3oiJ");
            Log(Application.unityVersion);
            
            
            ab = AssetBundle.LoadFromFile(Application.dataPath + "/Managed/Mods/halfmod.assets");
            
            UnityEngine.Object[] meme = ab.LoadAllAssets();
            foreach (Object m1 in meme)
            {
                Log("found asset with name " + m1.name + " of type " + m1.GetType());
            }
            
            wingshadeMat = ab.LoadAllAssets<Material>()[1];
            wingshadeTex = textureReadHack(ab.LoadAllAssets<Texture2D>()[1]);
            wingshadeTex.Apply();
            
            CanvasUtil.TrajanBold = ab.LoadAllAssets<Font>()[0];
            CanvasUtil.TrajanNormal = ab.LoadAllAssets<Font>()[0];
            wingshadeTTF = ab.LoadAllAssets<Font>()[0];
            
            
            
            foreach (Text textComp in UIManager.instance.gameObject.GetComponentsInChildren<Text>(true))
            {
                textComp.font = wingshadeTTF;
            }
            
            wingshadeTMP = ScriptableObject.CreateInstance<TMP_FontAsset>();
            wingshadeTMP.atlas = wingshadeTex;
            wingshadeTMP.material = wingshadeMat;
            
            //UIManager.instance.gameObject.PrintSceneHierarchyTree("uitrash.txt");
            
            
            
            wingshadeTMP.AddFaceInfo(new FaceInfo()
            {
                Name = "Wingshade",
                PointSize = 262f,
                Scale = 0.6825f,
                CharacterCount = 77,
                LineHeight = 474.5f,
                Baseline = 0f,
                Ascender = 160.5f,
                CapHeight = 0,
                Descender = -52.4375f,
                CenterLine = 0f,
                SuperscriptOffset = 208.5f,
                SubscriptOffset = -38.890625f,
                SubSize = 0.5f,
                Underline = -38.890625f,
                UnderlineThickness = 13.048828f,
                Padding = 5f,
                TabWidth = 0f,
                AtlasWidth = 2048,
                AtlasHeight = 2048
            });
            //wingshadeTMP.fontWeights = null;
            
            /*
            wingshadeTMP.boldSpacing = 7f;
            wingshadeTMP.boldStyle = 0.75f;
            wingshadeTMP.normalStyle = 1f;
            wingshadeTMP.normalSpacingOffset = 0f;
            wingshadeTMP.italicStyle = 35;
            wingshadeTMP.tabSize = 10;
            
            */
            
            wingshadeTMP.AddKerningInfo(new KerningTable());
            
            consts.fixGlyphData();
            wingshadeTMP.AddGlyphInfo(consts.glyphData);
            wingshadeTMP.ReadFontDefinition();
            wingshadeTMP.fontAssetType = TMP_FontAsset.FontAssetTypes.Bitmap;
            Log("Read font definition without errors");
            
            ModHooks.Instance.AfterSavegameLoadHook += saveGame;
            ModHooks.Instance.NewGameHook += newGame;

            On.TMPro.TextMeshPro.Awake += forceLoadWingshade;
            On.TMPro.TextMeshPro.LoadFontAsset += loadWingshade;
            UnityEngine.SceneManagement.SceneManager.sceneLoaded += returnToMenuScene;
        }
        
        private void returnToMenuScene(Scene to, LoadSceneMode loadSceneMode)
        {            
            if (to.name != "Menu_Title") return;
            
            foreach (Text textComp in UIManager.instance.gameObject.GetComponentsInChildren<Text>(true))
            {
                textComp.font = wingshadeTTF;
            }
        }

        private void newGame()
        {
            GameManager.instance.gameObject.AddComponent<tmp_memes>();
            GameManager.instance.gameObject.AddComponent<nuke_meme>();
        }

        private void saveGame(SaveGameData data)
        {
            newGame();
        }

        private void forceLoadWingshade(TextMeshPro.orig_Awake orig, TMPro.TextMeshPro self)
        {
            Log("force loading stuff");
            orig(self);
            self.UpdateFontAsset();
        }

        
        private void loadWingshade(On.TMPro.TextMeshPro.orig_LoadFontAsset orig, TMPro.TextMeshPro self)
        {
            Log("Loading wingshade. this should run after force load");
            self.SetAttr<TMP_FontAsset>("m_fontAsset", wingshadeTMP);
            self.SetAttr<TMP_FontAsset>("m_currentFontAsset", wingshadeTMP);
            orig(self);
            Log("Loaded wingshade in theory.");
        }

        public override string GetVersion()
        {
            return VERSION_NAMES[rng.Next(0, VERSION_NAMES.Length)];
        }
        
        public static Texture2D textureReadHack(Texture2D in_tex)
        {
            RenderTexture temporary = RenderTexture.GetTemporary(in_tex.width, in_tex.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
            Graphics.Blit(in_tex, temporary);
            RenderTexture active = RenderTexture.active;
            RenderTexture.active = temporary;
            Texture2D texture2D = new Texture2D(in_tex.width, in_tex.height);
            texture2D.ReadPixels(new Rect(0f, 0f, (float)temporary.width, (float)temporary.height), 0, 0);
            texture2D.Apply();
            RenderTexture.active = active;
            RenderTexture.ReleaseTemporary(temporary);
            return texture2D;
        }


        public void Unload()
        {
            try
            {
                On.TMPro.TextMeshPro.Awake -= forceLoadWingshade;
                On.TMPro.TextMeshPro.LoadFontAsset -= loadWingshade;
                UnityEngine.SceneManagement.SceneManager.sceneLoaded -= returnToMenuScene;
            }
            catch (Exception e)
            {
                // ignored
            }
        }
    }
}