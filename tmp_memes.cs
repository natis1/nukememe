using System.Collections;
using ModCommon;
using UnityEngine;
using UnityEngine.UI;

namespace halfmod
{
    public class tmp_memes : MonoBehaviour
    {
        private IEnumerator Start()
        {
            while (HeroController.instance == null)
            {
                yield return null;
            }
            
            foreach (TextMesh t in HeroController.instance.geoCounter.gameObject.transform.parent.GetComponentsInChildren<TextMesh>(true))
            {
                t.font = halfmod.wingshadeTTF;
                t.gameObject.GetComponent<MeshRenderer>().sharedMaterial = halfmod.wingshadeTTF.material;
            }
        }
    }
}