﻿using System.Collections;
using System.IO;
using ModCommon;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

namespace halfmod
{
    public class nuke_meme : MonoBehaviour
    {
        private bool halfEnding = false;
        
        public void Start()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += meme;
        }

        private void OnDestroy()
        {
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged -= meme;
        }

        private void meme(Scene from, Scene to)
        {
            log("going from scene " + from.name + " to " + to.name);

            if (to.name == "Room_Final_Boss_Core")
            {
                
                //StartCoroutine(loadNukeShit());
            }
            
            if (to.name.StartsWith("Cinematic_Ending_D") || to.name.Contains("Cinematic_Ending_E"))
            {
                halfEnding = true;
                StartCoroutine(loadEndingDelay());
            } else if (to.name.StartsWith("Cinematic_Ending"))
            {
                halfEnding = false;
                StartCoroutine(loadEndingDelay());
            }
        }

        private void loadEnding()
        {
            string videoPath = "";
            if (halfEnding)
            {
                videoPath = Application.dataPath + "/Managed/Mods/halfmod.webm";
            }
            else
            {
                videoPath = Application.dataPath + "/Managed/Mods/nuke.webm";
            }
            log("Loading video from path " + videoPath);

            if (!File.Exists(videoPath))
            {
                log("Custom cutscene NOT installed!!!!!");
                log("Falling back to vanilla cutscenes because why not?");
                GameObject o = new GameObject("redwingEnding");
                return;
            }

            GameObject oldRenderer = GameObject.Find("Cinematic Player");
            GameObject newVideoPlayer = new GameObject("redwingEnding", typeof(VideoPlayer), typeof(AudioSource),
                typeof(video_switcher));

            newVideoPlayer.transform.position = oldRenderer.transform.position;
            newVideoPlayer.transform.rotation = oldRenderer.transform.rotation;

            AudioSource a = newVideoPlayer.GetComponent<AudioSource>();
            a.volume = (GameManager.instance.gameSettings.masterVolume *
                        GameManager.instance.gameSettings.soundVolume * 0.01f);
            a.playOnAwake = false;
            a.Stop();
            a.bypassEffects = true;
            
            VideoPlayer v = newVideoPlayer.GetOrAddComponent<VideoPlayer>();
            
            v.audioOutputMode = VideoAudioOutputMode.AudioSource;
            v.SetTargetAudioSource(0, a);

            v.url = videoPath;
            v.renderMode = VideoRenderMode.CameraNearPlane;
            v.isLooping = false;
            v.playOnAwake = false;
            v.waitForFirstFrame = true;
            
            v.audioOutputMode = VideoAudioOutputMode.Direct;
            v.targetCamera = Camera.current;
            v.aspectRatio = VideoAspectRatio.FitInside;
            
            v.Prepare();
            DestroyImmediate(oldRenderer);
        }
        
        private IEnumerator loadEndingDelay()
        {
            //yield return new WaitForSeconds(0.5f);
            yield return null;
            loadEnding();
            
            VideoPlayer rwe = GameObject.Find("redwingEnding").GetComponent<VideoPlayer>();
            if (rwe == null)
            {
                yield break;
            }
            while (!rwe.isPrepared)
            {
                log("Preparing video");
                yield return new WaitForSeconds(0.5f);
            }
            rwe.Play();
            GameObject.Find("redwingEnding").GetComponent<AudioSource>().Play();
        }
        
        
        IEnumerator loadNukeShit() {
            float waitTime = 0f;
            while (waitTime < 3.0f)
            {
                waitTime += Time.deltaTime;
                yield return null;
            }
            
            UnityEngine.SceneManagement.SceneManager.LoadScene("Cinematic_Ending_D");
        }

        IEnumerator dumpNukeShit()
        {
            float waitTime = 0f;
            while (waitTime < 3.0f)
            {
                waitTime += Time.deltaTime;
                yield return null;
            }
            // code to dump stuff.
            
            
        }
        
        
        public void log(string str)
        {
            Modding.Logger.Log("[Nuke Meme] " + str);
        }
        
    }
}